-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: artexamregister
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `username` varchar(45) NOT NULL COMMENT '用户名',
  `password` varchar(45) NOT NULL COMMENT '密码',
  `name` varchar(45) NOT NULL COMMENT '姓名',
  `type` int NOT NULL COMMENT '管理员类型：1为省考试院管理员，2为高校管理员',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('admin','admin','陈阳（省考试院）',1),('admin2','admin2','张力（湖北美术学院）',2),('admin3','admin3','高启强（武汉音乐学院）',3),('admin4','admin4','安欣（华中科技大学）',4),('admin5','admin5','唐小龙（武汉纺织大学）',5);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_art`
--

DROP TABLE IF EXISTS `class_art`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_art` (
  `registration_number` varchar(14) NOT NULL COMMENT '高考报名号',
  `grade` int NOT NULL DEFAULT '0' COMMENT '成绩',
  `class_name` varchar(45) NOT NULL DEFAULT '美术与设计类',
  PRIMARY KEY (`registration_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='美术与设计类报名表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_art`
--

LOCK TABLES `class_art` WRITE;
/*!40000 ALTER TABLE `class_art` DISABLE KEYS */;
INSERT INTO `class_art` VALUES ('21429401152011',90,'美术与设计类'),('21429401152013',0,'美术与设计类');
/*!40000 ALTER TABLE `class_art` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_broadcast`
--

DROP TABLE IF EXISTS `class_broadcast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_broadcast` (
  `registration_number` varchar(14) NOT NULL COMMENT '高考报名号',
  `grade` int NOT NULL DEFAULT '0' COMMENT '成绩',
  `class_name` varchar(45) NOT NULL DEFAULT '播音与主持类',
  PRIMARY KEY (`registration_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='播音与主持类报名表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_broadcast`
--

LOCK TABLES `class_broadcast` WRITE;
/*!40000 ALTER TABLE `class_broadcast` DISABLE KEYS */;
INSERT INTO `class_broadcast` VALUES ('21429401152016',89,'播音与主持类');
/*!40000 ALTER TABLE `class_broadcast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_calligraphy`
--

DROP TABLE IF EXISTS `class_calligraphy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_calligraphy` (
  `registration_number` varchar(14) NOT NULL COMMENT '高考报名号',
  `grade` int NOT NULL DEFAULT '0' COMMENT '成绩',
  `class_name` varchar(45) NOT NULL DEFAULT '书法类',
  PRIMARY KEY (`registration_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='书法类报名表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_calligraphy`
--

LOCK TABLES `class_calligraphy` WRITE;
/*!40000 ALTER TABLE `class_calligraphy` DISABLE KEYS */;
INSERT INTO `class_calligraphy` VALUES ('21429401152017',86,'书法类');
/*!40000 ALTER TABLE `class_calligraphy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_dance`
--

DROP TABLE IF EXISTS `class_dance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_dance` (
  `registration_number` varchar(14) NOT NULL COMMENT '高考报名号',
  `grade` int NOT NULL DEFAULT '0' COMMENT '成绩',
  `class_name` varchar(45) NOT NULL DEFAULT '舞蹈类',
  PRIMARY KEY (`registration_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='舞蹈类报名表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_dance`
--

LOCK TABLES `class_dance` WRITE;
/*!40000 ALTER TABLE `class_dance` DISABLE KEYS */;
INSERT INTO `class_dance` VALUES ('21429401151102',86,'舞蹈类');
/*!40000 ALTER TABLE `class_dance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_music`
--

DROP TABLE IF EXISTS `class_music`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_music` (
  `registration_number` varchar(14) NOT NULL COMMENT '高考报名号',
  `direction1` int DEFAULT '0' COMMENT '音乐表演方向',
  `grade1` int NOT NULL DEFAULT '0' COMMENT '音乐表演方向成绩',
  `direction2` int DEFAULT '0' COMMENT '音乐教育方向',
  `grade2` int NOT NULL DEFAULT '0' COMMENT '音乐教育方向成绩',
  `class_name` varchar(45) NOT NULL DEFAULT '音乐类',
  PRIMARY KEY (`registration_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='音乐类报名表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_music`
--

LOCK TABLES `class_music` WRITE;
/*!40000 ALTER TABLE `class_music` DISABLE KEYS */;
INSERT INTO `class_music` VALUES ('21429401152010',0,0,1,80,'音乐类');
/*!40000 ALTER TABLE `class_music` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_perform`
--

DROP TABLE IF EXISTS `class_perform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_perform` (
  `registration_number` varchar(14) NOT NULL COMMENT '高考报名号',
  `direction1` int DEFAULT '0' COMMENT '戏剧影视表演方向',
  `grade1` int NOT NULL DEFAULT '0' COMMENT '戏剧影视表演方向成绩',
  `direction2` int DEFAULT '0' COMMENT '服装表演方向',
  `grade2` int NOT NULL DEFAULT '0' COMMENT '服装表演方向成绩',
  `direction3` int DEFAULT '0' COMMENT '戏剧影视导演方向',
  `grade3` int NOT NULL DEFAULT '0' COMMENT '戏剧影视导演方向成绩',
  `class_name` varchar(45) NOT NULL DEFAULT '表（导）演类',
  PRIMARY KEY (`registration_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='表（导）演类报名表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_perform`
--

LOCK TABLES `class_perform` WRITE;
/*!40000 ALTER TABLE `class_perform` DISABLE KEYS */;
INSERT INTO `class_perform` VALUES ('21429401152019',1,0,1,0,0,0,'表（导）演类'),('21429401153110',1,88,0,0,1,96,'表（导）演类');
/*!40000 ALTER TABLE `class_perform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `config` (
  `exam` varchar(45) NOT NULL,
  `admission` int NOT NULL DEFAULT '0',
  `grade` int NOT NULL DEFAULT '0',
  `art` int NOT NULL DEFAULT '1' COMMENT '艺术类报名',
  PRIMARY KEY (`exam`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统管理表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES ('2024艺考',1,1,1);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `registration_number` varchar(14) NOT NULL COMMENT '高考报名号',
  `password` varchar(45) NOT NULL COMMENT '密码',
  `id_number` varchar(18) NOT NULL COMMENT '身份证号',
  `name` varchar(20) NOT NULL COMMENT '姓名',
  `sex` varchar(2) NOT NULL COMMENT '性别',
  `age` int NOT NULL COMMENT '年龄',
  `birth` date NOT NULL COMMENT '出生年月',
  `state` varchar(45) NOT NULL DEFAULT '未操作' COMMENT '文化课已报名，已现场确认，艺术类已报名，已缴费，已查询下载准考证，已查询成绩',
  PRIMARY KEY (`registration_number`),
  UNIQUE KEY `registration_number_UNIQUE` (`registration_number`),
  UNIQUE KEY `id_number_UNIQUE` (`id_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES ('21429401151102','235619','620000200206235619','小帅','男',21,'2002-06-23','已查询下载准考证'),('21429401152010','291568','420000200306291568','小徐','女',20,'2003-06-29','已缴费'),('21429401152011','281325','320000200504281325','小丽','女',18,'2005-04-28','已查询下载准考证'),('21429401152013','241256','420000200511241256','小熊','男',18,'2005-11-24','艺术类已报名'),('21429401152016','122356','420000200308122356','小张','男',20,'2003-08-12','艺术类已报名'),('21429401152017','249512','422000200212249512','小强','男',21,'2002-12-24','艺术类已报名'),('21429401152019','231589','320000200205231589','小静','女',21,'2002-05-23','艺术类已报名'),('21429401153110','035623','420000200305035623','小美','女',20,'2003-05-03','已查询下载准考证');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'artexamregister'
--

--
-- Dumping routines for database 'artexamregister'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-20 22:15:13
