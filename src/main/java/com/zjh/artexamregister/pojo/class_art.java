package com.zjh.artexamregister.pojo;

public class class_art {
    private String registration_number;//高考报名号
    private int grade;//成绩
    private String class_name;//科目类名

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    @Override
    public String toString() {
        return "class_art{" +
                ", registration_number='" + registration_number + '\'' +
                ", grade=" + grade +
                ", class_name='" + class_name + '\'' +
                '}';
    }
}
