package com.zjh.artexamregister.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.Date;

public class Student {
    private String registration_number;//高考报名号
    @JsonIgnore//让springmvc把当前对象转换成json字符串的时候,忽略password,最终的json字符串中就没有password这个属性了
    private String password;//密码
    private String id_number;//身份证号
    private String name;//姓名
    private String sex;//性别
    private int age;//年龄
    private LocalDate birth;//出生日期
    private String state;//状态

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getRegistration_number(){
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId_number(){
        return id_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate date) {
        this.birth = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Student{" +
                ", registration_number='" + registration_number + '\'' +
                ", password='" + password + '\'' +
                ", id_number='" + id_number + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", birth=" + birth +
                ", state=" + state +
                '}';
    }
}
