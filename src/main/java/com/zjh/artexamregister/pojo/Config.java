package com.zjh.artexamregister.pojo;

import lombok.Data;

@Data
public class Config {
    private String exam;
    private int admission;
    private int grade;
    private int art;
}
