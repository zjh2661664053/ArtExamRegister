package com.zjh.artexamregister.pojo;

public class class_music {
    private String registration_number;//高考报名号
    private int direction1;//音乐表演方向
    private int grade1;//音乐表演方向成绩
    private int direction2;//音乐教育方向
    private int grade2;//音乐教育方向成绩
    private String class_name;//科目类名

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public int getDirection1() {
        return direction1;
    }

    public void setDirection1(int direction1) {
        this.direction1 = direction1;
    }

    public int getGrade1() {
        return grade1;
    }

    public void setGrade1(int grade1) {
        this.grade1 = grade1;
    }

    public int getDirection2() {
        return direction2;
    }

    public void setDirection2(int direction2) {
        this.direction2 = direction2;
    }

    public int getGrade2() {
        return grade2;
    }

    public void setGrade2(int grade2) {
        this.grade2 = grade2;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    @Override
    public String toString() {
        return "class_music{" +
                ", registration_number='" + registration_number + '\'' +
                ", direction1=" + direction1 +
                ", grade1=" + grade1 +
                ", direction2=" + direction2 +
                ", grade2=" + grade2 +
                ", class_name='" + class_name + '\'' +
                '}';
    }
}
