package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.class_art;
import com.zjh.artexamregister.pojo.class_perform;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface Class_performMapper {
    @Select("select * from class_perform where registration_number=#{registration_Number}")
    class_perform FindByregistration_number(String registration_Number);

    @Select("select * from class_perform")
    List<class_perform> FindPerformExamInfo();

    @Update("update class_perform set grade1=#{grade1}, grade2=#{grade2}, grade3=#{grade3} where registration_number=#{registration_number}")
    void setGrade(class_perform classPerform);

    @Delete("delete from class_perform where registration_number=#{registrationNumber}")
    void Delete(String registrationNumber);
}
