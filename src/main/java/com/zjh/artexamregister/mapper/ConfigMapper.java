package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.Config;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface ConfigMapper {
    @Select("select * from config where exam='2024艺考'")
    Config GetConfig();

    @Update("update config set admission=1 where exam='2024艺考'")
    void OpenAdmission();

    @Update("update config set grade=1 where exam='2024艺考'")
    void OpenGrade();

    @Update("update config set admission=0 where exam='2024艺考'")
    void CloseAdmission();

    @Update("update config set grade=0 where exam='2024艺考'")
    void CloseGrade();

    @Update("update config set art=0 where exam='2024艺考'")
    void CloseArt();

    @Update("update config set art=1 where exam='2024艺考'")
    void OpenArt();
}
