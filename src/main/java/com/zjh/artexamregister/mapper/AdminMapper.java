package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.Admin;
import com.zjh.artexamregister.pojo.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface AdminMapper {
    @Select("select * from admin where username=#{username}")
    Admin FindByUserName(String username);

    @Insert("insert into student(registration_number, password, id_number, name, sex, age, birth, state) values (#{registration_number}, #{password}, #{id_number}, #{name}, #{sex}, #{age}, #{birth}, #{state})")
    void InsertStudent(Student newstudent);

    @Select("select * from student")
    List<Student> FindStudent();

    @Update("update student set registration_number=#{registration_number}, password=#{password}, id_number=#{id_number}, name=#{name}, sex=#{sex}, age=#{age}, birth=#{birth}, state=#{state} where registration_number=#{registration_number}")
    void UpdateStudent(Student updatestudent);
}
