package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.class_art;
import com.zjh.artexamregister.pojo.class_calligraphy;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface Class_calligraphyMapper {
    @Select("select * from class_calligraphy where registration_number=#{registration_Number}")
    class_calligraphy FindByregistration_number(String registration_Number);

    @Select("select * from class_calligraphy")
    List<class_calligraphy> FindCalligraphyExamInfo();

    @Update("update class_calligraphy set grade=#{grade} where registration_number=#{registration_number}")
    void setGrade(class_calligraphy classCalligraphy);

    @Delete("delete from class_calligraphy where registration_number=#{registrationNumber}")
    void Delete(String registrationNumber);
}
