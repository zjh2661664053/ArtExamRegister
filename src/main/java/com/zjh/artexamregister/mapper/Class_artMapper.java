package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.class_art;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface Class_artMapper {
    @Select("select * from class_art where registration_number=#{registration_number}")
    class_art FindByregistration_number(String registration_number);

    @Select("select * from class_art")
    List<class_art> FindArtExamInfo();

    @Update("update class_art set grade=#{grade} where registration_number=#{registration_number}")
    void setGrade(class_art classArt);

    @Delete("delete from class_art where registration_number=#{registrationNumber}")
    void Delete(String registrationNumber);
}
