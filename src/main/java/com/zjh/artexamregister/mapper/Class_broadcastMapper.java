package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.class_art;
import com.zjh.artexamregister.pojo.class_broadcast;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface Class_broadcastMapper {
    @Select("select * from class_broadcast where registration_number=#{registrationNumber}")
    class_broadcast FindByregistration_number(String registrationNumber);

    @Select("select * from class_broadcast")
    List<class_broadcast> FindBroadCastExamInfo();

    @Update("update class_broadcast set grade=#{grade} where registration_number=#{registration_number}")
    void setGrade(class_broadcast classBroadcast);

    @Delete("delete from class_broadcast where registration_number=#{registrationNumber}")
    void Delete(String registrationNumber);
}
