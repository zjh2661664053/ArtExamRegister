package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.Student;
import org.apache.ibatis.annotations.*;

@Mapper
public interface StudentMapper {
    //根据高考报名号查询考生
    @Select("select * from student where registration_number=#{registration_number}")
    Student FindByregistration_number(String registration_number);

    @Insert("insert into class_art(registration_number) values (#{registration_number})")
    void SignUpArt_class_art(String registration_number);

    @Insert("insert into class_broadcast(registration_number) values (#{registration_number})")
    void SignUpArt_class_broadcast(String registration_number);

    @Insert("insert into class_calligraphy(registration_number) values (#{registration_number})")
    void SignUpArt_class_calligraphy(String registration_number);

    @Insert("insert into class_dance(registration_number) values (#{registration_number})")
    void SignUpArt_class_dance(String registration_number);

    @Insert("insert into class_music(registration_number, direction1, direction2) values (#{registration_number}, #{direction1}, #{direction2})")
    void SignUpArt_class_music(String registration_number, int direction1, int direction2);

    @Insert("insert into class_perform(registration_number, direction1, direction2, direction3) values (#{registration_number}, #{direction1}, #{direction2}, #{direction3})")
    void SignUpArt_class_perform(String registration_number, int direction1, int direction2, int direction3);

    @Update("update student set state=#{s} where registration_number=#{registration_number}")
    void SetStudentState(String registration_number, String s);

    @Delete("delete from student where registration_number=#{registration_number}")
    void DeleteStudent(String registrationNumber);
}
