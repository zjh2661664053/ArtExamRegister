package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.class_art;
import com.zjh.artexamregister.pojo.class_dance;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface Class_danceMapper {
    @Select("select * from class_dance where registration_number=#{registration_Number}")
    class_dance FindByregistration_number(String registration_Number);

    @Select("select * from class_dance")
    List<class_dance> FindDanceExamInfo();

    @Update("update class_dance set grade=#{grade} where registration_number=#{registration_number}")
    void setGrade(class_dance classDance);

    @Delete("delete from class_dance where registration_number=#{registrationNumber}")
    void Delete(String registrationNumber);
}
