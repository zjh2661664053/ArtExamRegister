package com.zjh.artexamregister.mapper;

import com.zjh.artexamregister.pojo.class_art;
import com.zjh.artexamregister.pojo.class_music;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface Class_musicMapper {
    @Select("select * from class_music where registration_number=#{registration_Number}")
    class_music FindByregistration_number(String registration_Number);

    @Select("select * from class_music")
    List<class_music> FindMusicExamInfo();

    @Update("update class_music set grade1=#{grade1}, grade2=#{grade2} where registration_number=#{registration_number}")
    void setGrade(class_music classMusic);

    @Delete("delete from class_music where registration_number=#{registrationNumber}")
    void Delete(String registrationNumber);
}
