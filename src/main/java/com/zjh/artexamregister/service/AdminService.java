package com.zjh.artexamregister.service;

import com.zjh.artexamregister.pojo.*;

import java.util.List;
import java.util.Map;

public interface AdminService {
    Admin FindByUserName(String username);

    String Login(String username, String password);

    String InsertStudent(Map<String, String> student);

    String DeleteStudent(String registration_number);

    List<Student> FindStudent();

    String UpdateStudent(Map<String, String> student);

    List<Map<String, Object>> FindArtExamInfo();

    List<Map<String, Object>> FindBroadcastExamInfo();

    List<Map<String, Object>> FindCalligraphyExamInfo();

    List<Map<String, Object>> FindDanceExamInfo();

    List<Map<String, Object>> FindMusicExamInfo();

    List<Map<String, Object>> FindPerformExamInfo();

    String SetGrade(Map<String, String> gradeInfo);

    Config getConfig();

    void OpenAdmission();

    void OpenGrade();

    void CloseAdmission();

    void CloseGrade();

    void OpenArt();

    void CloseArt();
}
