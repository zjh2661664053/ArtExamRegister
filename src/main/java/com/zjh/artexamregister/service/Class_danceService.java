package com.zjh.artexamregister.service;

import com.zjh.artexamregister.mapper.Class_broadcastMapper;
import com.zjh.artexamregister.pojo.class_dance;
import org.springframework.beans.factory.annotation.Autowired;

public interface Class_danceService {
    class_dance findByregistration_number(String registration_number);
}
