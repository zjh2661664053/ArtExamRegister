package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.Class_artMapper;
import com.zjh.artexamregister.pojo.class_art;
import com.zjh.artexamregister.service.Class_artService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Class_artServiceImpl implements Class_artService {
    @Autowired
    private Class_artMapper classArtMapper;
    @Override
    public class_art findByregistration_number(String registration_number) {
        return classArtMapper.FindByregistration_number(registration_number);
    }
}
