package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.*;
import com.zjh.artexamregister.pojo.*;
import com.zjh.artexamregister.service.StudentService;
import com.zjh.artexamregister.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Student FindByregistration_number(String registration_number) {
        return studentMapper.FindByregistration_number(registration_number);
    }

    @Override
    public void SetStudentState(String registration_number, String s) {
        studentMapper.SetStudentState(registration_number, s);
    }

    @Override
    public void DeleteStudent(String registrationNumber) {
        studentMapper.DeleteStudent(registrationNumber);
    }

    @Autowired
    Class_artMapper classArtMapper;
    @Autowired
    Class_broadcastMapper classBroadcastMapper;
    @Autowired
    Class_calligraphyMapper classCalligraphyMapper;
    @Autowired
    Class_danceMapper classDanceMapper;
    @Autowired
    Class_musicMapper classMusicMapper;
    @Autowired
    Class_performMapper classPerformMapper;
    @Override
    public Map StudentInfo(String registration_number) {
        Map<String, Object> response = new HashMap<>();

        Student student = studentMapper.FindByregistration_number(registration_number);
        class_art classArt = classArtMapper.FindByregistration_number(registration_number);
        class_broadcast class_broadcast = classBroadcastMapper.FindByregistration_number(registration_number);
        class_calligraphy class_calligraphy = classCalligraphyMapper.FindByregistration_number(registration_number);
        class_dance class_dance = classDanceMapper.FindByregistration_number(registration_number);
        class_music class_music = classMusicMapper.FindByregistration_number(registration_number);
        class_perform class_perform = classPerformMapper.FindByregistration_number(registration_number);
        if(classArt!=null)
        {
            response.put("exam", classArt);
        } else if (class_broadcast!=null) {
            response.put("exam", class_broadcast);
        } else if (class_calligraphy!=null) {
            response.put("exam", class_calligraphy);
        } else if (class_dance!=null) {
            response.put("exam", class_dance);
        } else if (class_music!=null) {
            response.put("exam", class_music);
        } else if (class_perform!=null) {
            response.put("exam", class_perform);
        }
        response.put("student", student);
        return response;
    }

    @Override
    public String SignUpArt(String registration_number, Map<String, String> SignupData) {
        String type = null;
        int direction1 = 0, direction2=0, direction3=0;
        if (SignupData.containsKey("type")) {
            type = SignupData.get("type");
        }
        if (SignupData.containsKey("direction1")) {
            direction1 = Integer.parseInt(SignupData.get("direction1"));
        }
        if (SignupData.containsKey("direction2")) {
            direction2 = Integer.parseInt(SignupData.get("direction2"));
        }
        if (SignupData.containsKey("direction3")) {
            direction3 = Integer.parseInt(SignupData.get("direction3"));
        }
        int flag=0;
        class_art classArt = classArtMapper.FindByregistration_number(registration_number);
        class_broadcast classBroadcast = classBroadcastMapper.FindByregistration_number(registration_number);
        class_calligraphy classCalligraphy = classCalligraphyMapper.FindByregistration_number(registration_number);
        class_dance classDance = classDanceMapper.FindByregistration_number(registration_number);
        class_music classMusic = classMusicMapper.FindByregistration_number(registration_number);
        class_perform classPerform = classPerformMapper.FindByregistration_number(registration_number);
        if(classArt!=null)
            flag++;
        if(classBroadcast!=null)
            flag++;
        if(classCalligraphy!=null)
            flag++;
        if(classDance!=null)
            flag++;
        if(classMusic!=null)
            flag++;
        if(classPerform!=null)
            flag++;
        if(flag>0)
            return "重复报名";

        switch (type) {
            case "美术与设计类" -> {
                studentMapper.SignUpArt_class_art(registration_number);
            }
            case "播音与主持类" -> {
                studentMapper.SignUpArt_class_broadcast(registration_number);
            }
            case "书法类" -> {
                studentMapper.SignUpArt_class_calligraphy(registration_number);
            }
            case "舞蹈类" -> {
                studentMapper.SignUpArt_class_dance(registration_number);
            }
            case "音乐类" -> {
                if(direction1==0 && direction2==0)
                    return "报名失败";
                else
                    studentMapper.SignUpArt_class_music(registration_number, direction1, direction2);
            }
            case "表（导）演类" -> {
                if(direction1==0 && direction2==0 && direction3==0)
                    return "报名失败";
                else
                    studentMapper.SignUpArt_class_perform(registration_number, direction1, direction2, direction3);
            }
        }
        return "报名成功";
    }

    @Override
    public String UpdateArt(String registration_number, Map<String, String> signupData) {
        class_art classArt = classArtMapper.FindByregistration_number(registration_number);
        class_broadcast classBroadcast = classBroadcastMapper.FindByregistration_number(registration_number);
        class_calligraphy classCalligraphy = classCalligraphyMapper.FindByregistration_number(registration_number);
        class_dance classDance = classDanceMapper.FindByregistration_number(registration_number);
        class_music classMusic = classMusicMapper.FindByregistration_number(registration_number);
        class_perform classPerform = classPerformMapper.FindByregistration_number(registration_number);
        if(classArt!=null)
            classArtMapper.Delete(registration_number);
        if(classBroadcast!=null)
            classBroadcastMapper.Delete(registration_number);
        if(classCalligraphy!=null)
            classCalligraphyMapper.Delete(registration_number);
        if(classDance!=null)
            classDanceMapper.Delete(registration_number);
        if(classMusic!=null)
            classMusicMapper.Delete(registration_number);
        if(classPerform!=null)
            classPerformMapper.Delete(registration_number);
        return SignUpArt(registration_number, signupData);
    }

    @Override
    public String Login(String registration_number, String password) {
        Student student = studentMapper.FindByregistration_number(registration_number);
        //判断该考生是否存在
        if (student == null) {
            return "报名号错误";
        }
        //判断密码是否正确
        if (password.equals(student.getPassword())) {
            //登录成功
            return "登录成功";
        }
        return "密码错误";
    }
}
