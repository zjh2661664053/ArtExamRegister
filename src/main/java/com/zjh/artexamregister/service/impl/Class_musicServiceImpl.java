package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.Class_musicMapper;
import com.zjh.artexamregister.pojo.class_music;
import com.zjh.artexamregister.service.Class_musicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Class_musicServiceImpl implements Class_musicService {
    @Autowired
    private Class_musicMapper class_musicMapper;
    @Override
    public class_music findByregistration_number(String registration_Number) {
        return class_musicMapper.FindByregistration_number(registration_Number);
    }
}
