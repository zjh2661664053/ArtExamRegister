package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.Class_calligraphyMapper;
import com.zjh.artexamregister.pojo.class_calligraphy;
import com.zjh.artexamregister.service.Class_calligraphyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;

@Service
public class Class_calligraphyServiceImpl implements Class_calligraphyService {
    @Autowired
    private Class_calligraphyMapper classCalligraphyMapper;
    @Override
    public class_calligraphy findByregistration_number(String registration_Number) {
        return classCalligraphyMapper.FindByregistration_number(registration_Number);
    }
}
