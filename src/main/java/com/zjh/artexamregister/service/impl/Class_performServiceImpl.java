package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.Class_performMapper;
import com.zjh.artexamregister.pojo.class_perform;
import com.zjh.artexamregister.service.Class_performService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Class_performServiceImpl implements Class_performService {
    @Autowired
    private Class_performMapper classPerformMapper;
    @Override
    public class_perform findByregistration_number(String registration_Number) {
        return classPerformMapper.FindByregistration_number(registration_Number);
    }
}
