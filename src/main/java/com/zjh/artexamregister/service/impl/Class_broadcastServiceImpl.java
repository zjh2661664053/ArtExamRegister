package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.Class_broadcastMapper;
import com.zjh.artexamregister.pojo.class_broadcast;
import com.zjh.artexamregister.service.Class_broadcastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Class_broadcastServiceImpl implements Class_broadcastService {
    @Autowired
    private Class_broadcastMapper classBroadcastMapper;
    @Override
    public class_broadcast findByregistration_number(String registration_number) {
        return classBroadcastMapper.FindByregistration_number(registration_number);
    }
}
