package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.*;
import com.zjh.artexamregister.pojo.*;
import com.zjh.artexamregister.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin FindByUserName(String username) {
        return adminMapper.FindByUserName(username);
    }

    @Override
    public String Login(String username, String password) {
        Admin admin = adminMapper.FindByUserName(username);
        //判断该考生是否存在
        if (admin == null) {
            return "报名号错误";
        }
        //判断密码是否正确
        if (password.equals(admin.getPassword())) {
            //登录成功
            return "登录成功";
        }
        return "密码错误";
    }

    @Override
    public String InsertStudent(Map<String, String> student) {
        String registration_number = student.get("registration_number");
        String id_number = student.get("id_number");
        String name = student.get("name");
        String sex = student.get("sex");
        Student newstudent = new Student();
        Student student1 = studentMapper.FindByregistration_number(registration_number);
        if(registration_number.length()!=14 || id_number.length()!=18 || name=="" || sex=="")
            return "添加失败";

        if(student1!= null)
        {
            return "考生已存在";
        }

        newstudent.setRegistration_number(registration_number);
        newstudent.setId_number(id_number);
        String password = id_number.substring(id_number.length()-6);
        newstudent.setPassword(password);
        LocalDate birth = LocalDate.parse(id_number.substring(6, 10)+"-"+id_number.substring(10,12)+"-"+id_number.substring(12, 14));
        newstudent.setBirth(birth);
        Calendar calendar = Calendar.getInstance();
        int CurrentYear = calendar.get(Calendar.YEAR);
        int BirthYear = Integer.parseInt(id_number.substring(6, 10));
        int age = CurrentYear - BirthYear;
        newstudent.setAge(age);
        newstudent.setName(name);
        newstudent.setSex(sex);
        newstudent.setState("未操作");
        adminMapper.InsertStudent(newstudent);

        return "添加成功";
    }

    @Autowired
    StudentMapper studentMapper;
    @Override
    public String DeleteStudent(String registration_number) {
        Student student = studentMapper.FindByregistration_number(registration_number);
        if(student== null)
        {
            return "考生号错误";
        }
        class_art classArt = classArtMapper.FindByregistration_number(registration_number);
        class_broadcast classBroadcast = classBroadcastMapper.FindByregistration_number(registration_number);
        class_calligraphy classCalligraphy = classCalligraphyMapper.FindByregistration_number(registration_number);
        class_dance classDance = class_danceMapper.FindByregistration_number(registration_number);
        class_music classMusic = classMusicMapper.FindByregistration_number(registration_number);
        class_perform classPerform = classPerformMapper.FindByregistration_number(registration_number);
        if(classArt!=null)
            classArtMapper.Delete(registration_number);
        if(classBroadcast!=null)
            classBroadcastMapper.Delete(registration_number);
        if(classCalligraphy!=null)
            classCalligraphyMapper.Delete(registration_number);
        if(classDance!=null)
            class_danceMapper.Delete(registration_number);
        if(classMusic!=null)
            classMusicMapper.Delete(registration_number);
        if(classPerform!=null)
            classPerformMapper.Delete(registration_number);
        studentMapper.DeleteStudent(registration_number);
        return "删除成功";
    }

    @Override
    public List<Student> FindStudent() {
        return adminMapper.FindStudent();
    }

    @Override
    public String UpdateStudent(Map<String, String> student) {
        //数据验证
        String registration_number = student.get("registration_number");
        String id_number = student.get("id_number");
        String name = student.get("name");
        String sex = student.get("sex");
        String state = student.get("state");
        if(registration_number.length()!=14 || id_number.length()!=18 || name=="" || sex=="" || state=="")
            return "修改失败";

        Student updatestudent = new Student();
        updatestudent = studentMapper.FindByregistration_number(registration_number);
        if(updatestudent==null)
            return "考生号错误";

        updatestudent.setId_number(id_number);
        String password = id_number.substring(id_number.length()-6);
        LocalDate birth = LocalDate.parse(id_number.substring(6, 10)+"-"+id_number.substring(10,12)+"-"+id_number.substring(12, 14));
        updatestudent.setPassword(password);
        updatestudent.setBirth(birth);
        Calendar calendar = Calendar.getInstance();
        int CurrentYear = calendar.get(Calendar.YEAR);
        int BirthYear = Integer.parseInt(id_number.substring(6, 10));
        int age = CurrentYear - BirthYear;
        updatestudent.setAge(age);

        updatestudent.setName(name);
        updatestudent.setSex(sex);
        updatestudent.setState(state);
        adminMapper.UpdateStudent(updatestudent);
        return "更新成功";
    }

    @Autowired
    Class_artMapper classArtMapper;
    @Autowired
    Class_broadcastMapper classBroadcastMapper;
    @Autowired
    Class_calligraphyMapper classCalligraphyMapper;
    @Autowired
    Class_danceMapper class_danceMapper;
    @Autowired
    Class_musicMapper classMusicMapper;
    @Autowired
    Class_performMapper classPerformMapper;
    @Override
    public List<Map<String, Object>> FindArtExamInfo() {
        List<class_art> list = classArtMapper.FindArtExamInfo();
        List<String> registration_number = new ArrayList<>();
        List<Map<String, Object>> response = new ArrayList<>();
        Iterator<class_art> iterator1 = list.iterator();
        while(iterator1.hasNext())//获取该大类的所有考生的报名号存在List中
        {
            class_art classArt = iterator1.next();
            registration_number.add(classArt.getRegistration_number());
        }
        Iterator<String> iterator2 = registration_number.iterator();
        while(iterator2.hasNext())//根据List中的报名号在数据库中查找该考生的考试和个人信息存在Map中
        {
            Map<String, Object> result = new HashMap<>();
            String number = iterator2.next();
            class_art classArt = classArtMapper.FindByregistration_number(number);
            Student student = studentMapper.FindByregistration_number(number);
            result.put("exam", classArt);
            result.put("student", student);
            response.add(result);
        }
        return response;
    }

    @Override
    public List<Map<String, Object>> FindBroadcastExamInfo() {
        List<class_broadcast> list = classBroadcastMapper.FindBroadCastExamInfo();
        List<String> registration_number = new ArrayList<>();
        List<Map<String, Object>> response = new ArrayList<>();
        Iterator<class_broadcast> iterator1 = list.iterator();
        while(iterator1.hasNext())
        {
            class_broadcast class_broadcast = iterator1.next();
            registration_number.add(class_broadcast.getRegistration_number());
        }
        Iterator<String> iterator2 = registration_number.iterator();
        while(iterator2.hasNext())
        {
            Map<String, Object> result = new HashMap<>();
            String number = iterator2.next();
            class_broadcast class_broadcast = classBroadcastMapper.FindByregistration_number(number);
            Student student = studentMapper.FindByregistration_number(number);
            result.put("exam", class_broadcast);
            result.put("student", student);
            response.add(result);
        }
        return response;
    }

    @Override
    public List<Map<String, Object>> FindCalligraphyExamInfo() {
        List<class_calligraphy> list = classCalligraphyMapper.FindCalligraphyExamInfo();
        List<String> registration_number = new ArrayList<>();
        List<Map<String, Object>> response = new ArrayList<>();
        Iterator<class_calligraphy> iterator1 = list.iterator();
        while(iterator1.hasNext())
        {
            class_calligraphy class_calligraphy = iterator1.next();
            registration_number.add(class_calligraphy.getRegistration_number());
        }
        Iterator<String> iterator2 = registration_number.iterator();
        while(iterator2.hasNext())
        {
            Map<String, Object> result = new HashMap<>();
            String number = iterator2.next();
            class_calligraphy class_calligraphy = classCalligraphyMapper.FindByregistration_number(number);
            Student student = studentMapper.FindByregistration_number(number);
            result.put("exam", class_calligraphy);
            result.put("student", student);
            response.add(result);
        }
        return response;
    }

    @Override
    public List<Map<String, Object>> FindDanceExamInfo() {
        List<class_dance> list = class_danceMapper.FindDanceExamInfo();
        List<String> registration_number = new ArrayList<>();
        List<Map<String, Object>> response = new ArrayList<>();
        Iterator<class_dance> iterator1 = list.iterator();
        while(iterator1.hasNext())
        {
            class_dance class_dance = iterator1.next();
            registration_number.add(class_dance.getRegistration_number());
        }
        Iterator<String> iterator2 = registration_number.iterator();
        while(iterator2.hasNext())
        {
            Map<String, Object> result = new HashMap<>();
            String number = iterator2.next();
            class_dance class_dance = class_danceMapper.FindByregistration_number(number);
            Student student = studentMapper.FindByregistration_number(number);
            result.put("exam", class_dance);
            result.put("student", student);
            response.add(result);
        }
        return response;
    }

    @Override
    public List<Map<String, Object>> FindMusicExamInfo() {
        List<class_music> list = classMusicMapper.FindMusicExamInfo();
        List<String> registration_number = new ArrayList<>();
        List<Map<String, Object>> response = new ArrayList<>();
        Iterator<class_music> iterator1 = list.iterator();
        while(iterator1.hasNext())
        {
            class_music class_music = iterator1.next();
            registration_number.add(class_music.getRegistration_number());
        }
        Iterator<String> iterator2 = registration_number.iterator();
        while(iterator2.hasNext())
        {
            Map<String, Object> result = new HashMap<>();
            String number = iterator2.next();
            class_music class_music = classMusicMapper.FindByregistration_number(number);
            Student student = studentMapper.FindByregistration_number(number);
            result.put("exam", class_music);
            result.put("student", student);
            response.add(result);
        }
        return response;
    }

    @Override
    public List<Map<String, Object>> FindPerformExamInfo() {
        List<class_perform> list = classPerformMapper.FindPerformExamInfo();
        List<String> registration_number = new ArrayList<>();
        List<Map<String, Object>> response = new ArrayList<>();
        Iterator<class_perform> iterator1 = list.iterator();
        while(iterator1.hasNext())
        {
            class_perform class_perform = iterator1.next();
            registration_number.add(class_perform.getRegistration_number());
        }
        Iterator<String> iterator2 = registration_number.iterator();
        while(iterator2.hasNext())
        {
            Map<String, Object> result = new HashMap<>();
            String number = iterator2.next();
            class_perform class_perform = classPerformMapper.FindByregistration_number(number);
            Student student = studentMapper.FindByregistration_number(number);
            result.put("exam", class_perform);
            result.put("student", student);
            response.add(result);
        }
        return response;
    }

    @Override
    public String SetGrade(Map<String, String> gradeInfo) {
        String registration_number = gradeInfo.get("registration_number");
        String id_number = gradeInfo.get("id_number");
        String name = gradeInfo.get("name");
        String sex = gradeInfo.get("sex");
        String grade = gradeInfo.get("grade");
        String state = gradeInfo.get("state");
        String type = gradeInfo.get("type");

        if(registration_number.length()!=14 || id_number.length()!=18 || name=="" || sex=="" || grade=="" || state=="")
            return "修改失败";
        Student student = studentMapper.FindByregistration_number(registration_number);
        if(student==null)
            return "考生不存在";
        student.setId_number(id_number);
        student.setName(name);
        student.setSex(sex);
        student.setState(state);
        adminMapper.UpdateStudent(student);
        if(Objects.equals(type, "美术与设计类"))
        {
            class_art classArt = classArtMapper.FindByregistration_number(registration_number);
            classArt.setGrade(Integer.parseInt(grade));
            classArtMapper.setGrade(classArt);
            return "修改成功";
        }
        else if(Objects.equals(type, "音乐类"))
        {
            class_music classMusic = classMusicMapper.FindByregistration_number(registration_number);
            String direction1 = gradeInfo.get("direction1");
            String direction2 = gradeInfo.get("direction2");
            if(Objects.equals(direction1, "是"))
            {
                classMusic.setGrade1(Integer.parseInt(gradeInfo.get("grade1")));
            }
            if(Objects.equals(direction2, "是"))
            {
                classMusic.setGrade2(Integer.parseInt(gradeInfo.get("grade2")));
            }
            classMusicMapper.setGrade(classMusic);
            return "修改成功";
        }
        else if(Objects.equals(type, "舞蹈类"))
        {
            class_dance classDance = class_danceMapper.FindByregistration_number(registration_number);
            classDance.setGrade(Integer.parseInt(grade));
            class_danceMapper.setGrade(classDance);
            return "修改成功";
        }
        else if(Objects.equals(type, "表（导）演类"))
        {
            class_perform classPerform = classPerformMapper.FindByregistration_number(registration_number);
            String direction1 = gradeInfo.get("direction1");
            String direction2 = gradeInfo.get("direction2");
            String direction3 = gradeInfo.get("direction3");
            if(Objects.equals(direction1, "是"))
            {
                classPerform.setGrade1(Integer.parseInt(gradeInfo.get("grade1")));
            }
            if(Objects.equals(direction2, "是"))
            {
                classPerform.setGrade2(Integer.parseInt(gradeInfo.get("grade2")));
            }
            if(Objects.equals(direction3, "是"))
            {
                classPerform.setGrade3(Integer.parseInt(gradeInfo.get("grade3")));
            }
            classPerformMapper.setGrade(classPerform);
            return "修改成功";
        }
        else if(Objects.equals(type, "播音与主持类"))
        {
            class_broadcast classBroadcast = classBroadcastMapper.FindByregistration_number(registration_number);
            classBroadcast.setGrade(Integer.parseInt(grade));
            classBroadcastMapper.setGrade(classBroadcast);
            return "修改成功";
        }
        else if(Objects.equals(type, "书法类"))
        {
            class_calligraphy classCalligraphy = classCalligraphyMapper.FindByregistration_number(registration_number);
            classCalligraphy.setGrade(Integer.parseInt(grade));
            classCalligraphyMapper.setGrade(classCalligraphy);
            return "修改成功";
        }
        return "修改失败";
    }

    @Autowired
    ConfigMapper configMapper;
    @Override
    public Config getConfig() {
        return configMapper.GetConfig();
    }

    @Override
    public void OpenAdmission() {
        configMapper.OpenAdmission();
    }

    @Override
    public void OpenGrade() {
        configMapper.OpenGrade();
    }

    @Override
    public void CloseAdmission() {
        configMapper.CloseAdmission();
    }

    @Override
    public void CloseGrade() {
        configMapper.CloseGrade();
    }

    @Override
    public void OpenArt() {
        configMapper.OpenArt();
    }

    @Override
    public void CloseArt() {
        configMapper.CloseArt();
    }
}