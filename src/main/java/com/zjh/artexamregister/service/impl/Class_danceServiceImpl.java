package com.zjh.artexamregister.service.impl;

import com.zjh.artexamregister.mapper.Class_danceMapper;
import com.zjh.artexamregister.pojo.class_dance;
import com.zjh.artexamregister.service.Class_danceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Class_danceServiceImpl implements Class_danceService {
    @Autowired
    private Class_danceMapper class_danceMapper;
    @Override
    public class_dance findByregistration_number(String registration_number) {
        return class_danceMapper.FindByregistration_number(registration_number);
    }
}
