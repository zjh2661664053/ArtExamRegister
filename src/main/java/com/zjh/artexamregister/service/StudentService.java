package com.zjh.artexamregister.service;

import com.zjh.artexamregister.pojo.Student;

import java.util.Map;

public interface StudentService {
    Student FindByregistration_number(String registration_number);

    void SetStudentState(String registration_number, String s);

    void DeleteStudent(String registrationNumber);

    Map StudentInfo(String registration_number);

    String SignUpArt(String registration_number, Map<String, String> SignupData);

    String Login(String registration_number, String password);

    String UpdateArt(String registration_number, Map<String, String> signupData);
}
