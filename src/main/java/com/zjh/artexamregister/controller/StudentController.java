package com.zjh.artexamregister.controller;

import com.zjh.artexamregister.pojo.*;
import com.zjh.artexamregister.service.*;
import com.zjh.artexamregister.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @PostMapping("/login")//考生登录
    public Result<String> Login(String registration_number, String password) {
        String result = studentService.Login(registration_number, password);
        //登录成功
        if (result=="登录成功") {
            //登录成功
            Student student = studentService.FindByregistration_number(registration_number);
            Map<String, Object> claims = new HashMap<>();
            claims.put("registration_number", student.getRegistration_number());
            claims.put("name", student.getName());
            String token = JwtUtil.genToken(claims);
            return Result.success(token);
        }
        return Result.error("密码错误");
    }

    @GetMapping("studentInfo")//查询考生信息
    public Result<Student> StudentInfo(@RequestHeader(name = "Authorization") String token){
        Map<String, Object> map = JwtUtil.parseToken(token);
        String registration_number = (String) map.get("registration_number");
        Student student = studentService.FindByregistration_number(registration_number);
        return Result.success(student);
    }

    @PostMapping("/signupcultural")//高考文化课报名
    public Result<String> SignUpCultural(@RequestHeader(name = "Authorization") String token){
        Map<String, Object> map = JwtUtil.parseToken(token);
        String registration_number = (String) map.get("registration_number");
        studentService.SetStudentState(registration_number,"文化课已报名");
        return Result.success("后端输出：这里是高考文化课报名流程。。。");
    }

    @PostMapping("/confirm")//现场确认
    public Result<String> Confirm(@RequestHeader(name = "Authorization") String token){
        Map<String, Object> map = JwtUtil.parseToken(token);
        String registration_number = (String) map.get("registration_number");
        studentService.SetStudentState(registration_number, "已现场确认");
        return Result.success("后端输出：正在虹膜采集。。。");
    }

    @PostMapping("/signupart")//艺术类报名
    public Result SignUpArt(@RequestHeader(name = "Authorization")String token, @RequestBody Map<String, String> SignupData){
        Map<String, Object> map = JwtUtil.parseToken(token);
        String registration_number = (String) map.get("registration_number");
        String result = studentService.SignUpArt(registration_number, SignupData);
        if(result=="报名失败"){
            return Result.error("报名失败，请至少选择一个方向");
        }
        else if(result=="报名成功") {
            studentService.SetStudentState(registration_number, "艺术类已报名");
            return Result.success(result);
        }
        return Result.error(result);
   }

   @PostMapping("/updateart")//艺术类修改报名
   public Result UpdateArt(@RequestHeader(name = "Authorization")String token, @RequestBody Map<String, String> SignupData){
        Map<String, Object> map = JwtUtil.parseToken(token);
        String registration_number = (String)map.get("registration_number");
        String result = studentService.UpdateArt(registration_number, SignupData);
        if(result=="报名失败")
            return Result.error("报名失败，请至少选择一个方向");
        else if(result=="报名成功"){
            studentService.SetStudentState(registration_number, "艺术类已报名");
            return Result.success(result);
        }
        return Result.error(result);
   }

   @PostMapping("/pay")//缴费
   public Result<String> Pay(@RequestHeader(name = "Authorization") String token){
       Map<String, Object> map = JwtUtil.parseToken(token);
       String registration_number = (String) map.get("registration_number");
       studentService.SetStudentState(registration_number, "已缴费");
       return Result.success("后端输出：确认缴费。。。");
   }

   @GetMapping("/ExamInfo")//查询考生考试信息,兼查询成绩
   public Result<Map> ExamInfo(@RequestHeader(name = "Authorization") String token){
       Map<String, Object> map = JwtUtil.parseToken(token);
       String registration_number = (String) map.get("registration_number");
       Map<String, Object> response = studentService.StudentInfo(registration_number);
       return Result.success(response);
   }

    @PostMapping("/admission")//下载准考证
    public Result<String> Admission(@RequestHeader(name = "Authorization") String token){
        Map<String, Object> map = JwtUtil.parseToken(token);
        String registration_number = (String) map.get("registration_number");
        studentService.SetStudentState(registration_number, "已查询下载准考证");
        return Result.success("后端输出：下载准考证。。。");
    }
}
