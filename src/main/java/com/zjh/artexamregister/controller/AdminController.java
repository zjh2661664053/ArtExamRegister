package com.zjh.artexamregister.controller;

import com.zjh.artexamregister.pojo.*;
import com.zjh.artexamregister.service.AdminService;
import com.zjh.artexamregister.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @PostMapping("/login")
    public Result<String> Login(String username, String password){
        String result = adminService.Login(username, password);
        //登录成功
        if (result=="登录成功") {
            //登录成功
            Admin admin = adminService.FindByUserName(username);
            Map<String, Object> claims = new HashMap<>();
            claims.put("username", admin.getUsername());
            claims.put("name", admin.getName());
            String token = JwtUtil.genToken(claims);
            return Result.success(token);
        }
        return Result.error("密码错误");
    }

    @PostMapping("/insertstudent")
    public Result InsertStudent(@RequestBody Map<String, String> student) {
        String result = adminService.InsertStudent(student);
        if(result=="考生已存在")
            return Result.error(result);
        else if(result=="添加失败")
            return Result.error("添加失败，请完善考生信息");
        return Result.success(result);
    }

    @PostMapping("/deletestudent")
    public Result DeleteStudent(@RequestBody Map<String, String> student){
        String registration_number = student.get("registration_number");
        String result = adminService.DeleteStudent(registration_number);
        if(result=="考生号错误")
            return Result.error(result);
        return Result.success(result);
    }

    @GetMapping("/findstudent")//查询考生基本信息
    public Result<List<Student>> FindStudent(){
        return Result.success(adminService.FindStudent());
    }

    @PostMapping("/updatestudent")//修改考生基本信息
    public Result UpdateStudent(@RequestBody Map<String, String> student){
        String result = adminService.UpdateStudent(student);
        if(result=="考生号错误")
            return Result.error(result);
        else if(result=="修改失败")
            return Result.error("修改失败，请完善考生信息");
        else
            return Result.success(result);
    }

    @GetMapping("/findartexaminfo")//查看美术与设计类考试报名信息
    public Result<List<Map<String, Object>>> FindArtExamInfo(){
        return Result.success(adminService.FindArtExamInfo());
    }

    @GetMapping("/findbroadcastexaminfo")//查看播音与主持类考试报名信息
    public Result<List<Map<String, Object>>> FindBroadcastExamInfo(){
        return Result.success(adminService.FindBroadcastExamInfo());
    }

    @GetMapping("/findcalligraphyexaminfo")//查看书法类考试报名信息
    public Result<List<Map<String, Object>>> FindCalligraphyExamInfo(){
        return Result.success(adminService.FindCalligraphyExamInfo());
    }

    @GetMapping("/finddanceexaminfo")//查看舞蹈类考试报名信息
    public Result<List<Map<String, Object>>> FindDanceExamInfo(){
        return Result.success(adminService.FindDanceExamInfo());
    }

    @GetMapping("/findmusicexaminfo")//查看音乐类考试报名信息
    public Result<List<Map<String, Object>>> FindMusicExamInfo(){
        return Result.success(adminService.FindMusicExamInfo());
    }

    @GetMapping("/findperformexaminfo")//查看表（导）演类考试报名信息
    public Result<List<Map<String, Object>>> FindPerformExamInfo(){
        return Result.success(adminService.FindPerformExamInfo());
    }

    @GetMapping("/findadmininfo")//查找管理员信息
    public Result<Admin> FindAdminInfo(@RequestHeader(name = "Authorization") String token){
        Map<String, Object> map = JwtUtil.parseToken(token);
        String username = (String) map.get("username");
        Admin admin = adminService.FindByUserName(username);
        return Result.success(admin);
    }

    @PostMapping("/setgrade")//修改各类考生考试信息兼提供成绩
    public Result SetGrade(@RequestBody Map<String, String> GradeInfo){
        String result = adminService.SetGrade(GradeInfo);
        if(result=="修改失败")
            return Result.error("修改失败，请完善考生信息");
        else if(result=="考生不存在")
            return Result.error("考生不存在");
        else if(result=="修改失败")
            return Result.error("修改失败");
        return Result.success("修改成功");
    }

    @GetMapping("/getconfig")//查询考试设置
    public Result<Config> getConfig(){
        return Result.success(adminService.getConfig());
    }

    @PostMapping("/openadmission")//开放查询下载准考证
    public Result OpenAdmission(){
        adminService.OpenAdmission();
        return Result.success();
    }

    @PostMapping("/opengrade")//开放查询成绩
    public Result OpenGrade(){
        adminService.OpenGrade();
        return Result.success();
    }

    @PostMapping("/closeadmission")//关闭查询下载准考证
    public Result CloseAdmission(){
        adminService.CloseAdmission();
        return Result.success();
    }

    @PostMapping("/closegrade")//关闭查询成绩
    public Result CloseGraded(){
        adminService.CloseGrade();
        return Result.success();
    }

    @PostMapping("/openart")//开放艺术类报名
    public Result OpenArt(){
        adminService.OpenArt();
        return Result.success();
    }

    @PostMapping("/closeart")//关闭艺术类报名
    public Result CloseArt(){
        adminService.CloseArt();
        return Result.success();
    }
}