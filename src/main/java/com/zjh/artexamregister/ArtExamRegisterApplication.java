package com.zjh.artexamregister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtExamRegisterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArtExamRegisterApplication.class, args);
    }

}
